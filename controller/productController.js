const joi = require('@hapi/joi');

const vendorProductValidation = (value) => {
    const validationSchema = {
        productName: joi.string().required().max(225),
        productImage: joi.string().required(),
        productQuantity: joi.number().required(),
        productPrice: joi.number().required(),
        productDescription: joi.string().required().max(550),
        storeName: joi.string().required().max(225)
    };
    return joi.validate(value, validationSchema);
};

const vendorProductUpdateValidation = (value) => {
    const validationSchema = {
        productName: joi.string().required().max(225),
        productImage: joi.string().required(),
        productQuantity: joi.number().required(),
        productPrice: joi.number().required(),
        storeName: joi.string().required().max(225)
    };
    return joi.validate(value, validationSchema);
};

module.exports = { vendorProductValidation, vendorProductUpdateValidation };
