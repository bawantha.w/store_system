const { SECURE_KEY } = require('../constants/config');
const crypto = require('crypto');

const passwordEncrypt = (password) => {
    return crypto
        .createCipher('aes-256-ctr', SECURE_KEY)
        .update(password, 'utf8', 'hex');
};

const passwordDecrypt = (password) => {
    return crypto
        .createDecipher('aes-256-ctr', SECURE_KEY)
        .update(password, 'hex', 'utf8');
};

module.exports = { passwordEncrypt, passwordDecrypt };
