const joi = require('@hapi/joi');

function vendorStoreValidation(value) {
    const validationSchema = {
        storeName: joi.string().required().max(225),
        storeCategory: joi.string().required().max(50),
        storeDescription: joi.string().required().max(550)
    };
    return joi.validate(value, validationSchema);
}
function vendorStoreUpdateValidation(value) {
    const validationSchema = {
        storeName: joi.string().required().max(225),
        storeCategory: joi.string().required().max(50),
        storeDescription: joi.string().required().max(550)
    };
    return joi.validate(value, validationSchema);
}

module.exports = { vendorStoreValidation, vendorStoreUpdateValidation };
