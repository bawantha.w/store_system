const Joi = require('@hapi/joi');

function customerRegisterValidation(value) {
    const validationSchema = {
        customerName: Joi.string().required().min(2).max(50),
        customerEmail: Joi.string().required().min(5).max(225).email(),
        customerPassword: Joi.string().required().min(6).max(225),
        customerMobile: Joi.string().required().min(10).max(10),
        customerAddress: Joi.string().required().min(12).max(225)
    };
    return Joi.validate(value, validationSchema);
}

function customerLoginValidation(value) {
    const validationSchema = {
        customerEmail: Joi.string().required().min(5).max(225).email(),
        customerPassword: Joi.string().required().min(6).max(225)
    };
    return Joi.validate(value, validationSchema);
}

function customerUpdateValidation(value) {
    const validationSchema = {
        customerName: Joi.string().required().min(2).max(50),
        customerMobile: Joi.string().required().min(10).max(10),
        customerAddress: Joi.string().required().min(12).max(225)
    };
    return Joi.validate(value, validationSchema);
}

module.exports = {
    customerRegisterValidation,
    customerLoginValidation,
    customerUpdateValidation
};
