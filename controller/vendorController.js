const Joi = require('@hapi/joi');

function vendorRegisterValidation(value) {
    const validationSchema = {
        vendorName: Joi.string().required().min(2).max(50),
        vendorEmail: Joi.string().required().min(5).max(225).email(),
        vendorPassword: Joi.string().required().min(6).max(225),
        vendorMobile: Joi.string().required().min(10).max(10),
        vendorAddress: Joi.string().required().min(12).max(225)
    };
    return Joi.validate(value, validationSchema);
}

function vendorLoginValidation(value) {
    const validationSchema = {
        vendorEmail: Joi.string().required().min(5).max(225).email(),
        vendorPassword: Joi.string().required().min(6).max(225)
    };
    return Joi.validate(value, validationSchema);
}

function vendorUpdateValidation(value) {
    const validationSchema = {
        vendorName: Joi.string().required().min(2).max(50),
        vendorMobile: Joi.string().required().min(10).max(10),
        vendorAddress: Joi.string().required().min(12).max(225)
    };
    return Joi.validate(value, validationSchema);
}

module.exports = {
    vendorRegisterValidation,
    vendorLoginValidation,
    vendorUpdateValidation
};
