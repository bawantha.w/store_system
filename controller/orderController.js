const joi = require('@hapi/joi');
const { executeProductModel } = require('../model/productModel');

// VALIDATE REQUEST BODY INPUTS OF ORDER PLACE ROUTE
const orderValidation = (value) => {
    const validationSchema = {
        storeName: joi.string().required().max(225),
        orderItems: joi.required()
    };
    return joi.validate(value, validationSchema);
};

// CHECKING NECESSARY ITEM AVAILABILITY IN THE STORE
const checkingItemAvailable = async (
    productStoreName,
    itemName,
    itemQuantity
) => {
    const { storeName, productQuantity } = await executeProductModel.findOne({
        storeName: productStoreName,
        productName: itemName
    });
    if (!storeName) return false;
    if (productQuantity < itemQuantity) return false;

    return true;
};

const afterOrderProductUpdate = async (
    productStoreName,
    itemName,
    itemQuantity
) => {
    const productDetails = await executeProductModel.findOne({
        storeName: productStoreName,
        productName: itemName
    });

    // UPDATE PRODUCT QUANTITY
    await productDetails.updateOne({
        productQuantity: productDetails.productQuantity - itemQuantity
    });

    return true;
};

const totalCalculator = async (productStoreName, itemName, itemQuantity) => {
    const { productPrice } = await executeProductModel.findOne({
        storeName: productStoreName,
        productName: itemName
    });
    const totalAmount = productPrice * itemQuantity;
    return totalAmount;
};

module.exports = {
    orderValidation,
    checkingItemAvailable,
    totalCalculator,
    afterOrderProductUpdate
};
