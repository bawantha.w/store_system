const JWT = require('jsonwebtoken');
const { TOKEN_KEY } = require('../constants/config');

const emailFetcher = (accessToken) => {
    try {
        const token = accessToken.header('Authorization').split(' ')[0];

        if (token == null || token === ' ') return false;

        const decodedToken = JWT.verify(token, TOKEN_KEY);

        return decodedToken.vendorEmail;
    } catch (error) {
        return false;
    }
};

const customerEmailFetcher = (accessToken) => {
    try {
        const token = accessToken.header('Authorization').split(' ')[0];

        if (token == null || token === ' ') return false;

        const decodedToken = JWT.verify(token, TOKEN_KEY);

        return decodedToken.customerEmail;
    } catch (error) {
        return false;
    }
};

module.exports = { emailFetcher, customerEmailFetcher };
