const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const { PORT, DATABASE_URL } = require('./constants/config');
const swaggerJsDocs = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

mongoose
    .connect(DATABASE_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Connected to the mongodb database...');
    });

// ROUTES
const vendorRoutes = require('./routes/vendorRoutes');
const storeRoutes = require('./routes/storeRoutes');
const productRoutes = require('./routes/productRoutes');
const customerRoutes = require('./routes/customerRoutes');
const orderRoutes = require('./routes/orderRoutes');

// MIDDLEWARE
app.use(bodyParser.json());

// ROUTES MIDDLEWARE
app.use('/vendor', vendorRoutes);
app.use('/store', storeRoutes);
app.use('/product', productRoutes);
app.use('/customer', customerRoutes);
app.use('/order', orderRoutes);

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Store Server',
            description: 'Server Side API For Store System',
            contact: {
                name: 'Bawantha'
            },
            server: ['http://localhost:9000']
        }
    },
    apis: [
        './routes/vendorRoutes.js',
        './routes/storeRoutes.js',
        './routes/productRoutes.js',
        './routes/customerRoutes.js',
        './routes/orderRoutes.js'
    ]
};

const swaggerDocs = swaggerJsDocs(swaggerOptions);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.listen(PORT, () => {
    console.log('Server started successfully...');
});
