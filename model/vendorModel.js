const mongoose = require('mongoose');

const vendorSchema = new mongoose.Schema({
    vendorName: {
        type: String,
        required: true,
        max: 50
    },
    vendorEmail: {
        type: String,
        required: true,
        max: 225
    },
    vendorPassword: {
        type: String,
        required: true,
        max: 225
    },
    vendorMobile: {
        type: String,
        required: true,
        max: 10
    },
    vendorAddress: {
        type: String,
        required: true,
        max: 225
    }
});

const executeVendorModel = mongoose.model('vendors', vendorSchema);

module.exports = { executeVendorModel };
