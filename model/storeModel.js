const mongoose = require('mongoose');

const storeSchema = new mongoose.Schema({
    vendorEmail: {
        type: String,
        required: true,
        max: 225
    },
    storeName: {
        type: String,
        required: true,
        max: 225
    },
    storeCategory: {
        type: String,
        required: true,
        max: 50
    },
    storeDescription: {
        type: String,
        required: true,
        max: 550
    }
});

const executeStoreModel = mongoose.model('stores', storeSchema);

module.exports = { executeStoreModel };
