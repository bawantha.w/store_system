const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    customerEmail: {
        type: String,
        required: true,
        max: 225
    },
    storeName: {
        type: String,
        required: true,
        max: 225
    },
    orderProducts: {
        type: String,
        required: true
    },
    orderTotal: {
        type: Number,
        required: true
    },
    placedDate: {
        type: String,
        required: true
    }
});

const executeOrderModel = mongoose.model('orders', orderSchema);

module.exports = { executeOrderModel };
