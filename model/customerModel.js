const mongoose = require('mongoose');

const customerSchema = new mongoose.Schema({
    customerName: {
        type: String,
        required: true,
        max: 50
    },
    customerEmail: {
        type: String,
        required: true,
        max: 225
    },
    customerPassword: {
        type: String,
        required: true,
        max: 225
    },
    customerMobile: {
        type: String,
        required: true,
        max: 10
    },
    customerAddress: {
        type: String,
        required: true,
        max: 225
    }
});

const executeCustomerModel = mongoose.model('customers', customerSchema);

module.exports = { executeCustomerModel };
