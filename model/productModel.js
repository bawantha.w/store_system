const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required: true,
        max: 225
    },
    productImage: {
        type: String,
        required: true
    },
    productQuantity: {
        type: Number,
        required: true
    },
    productPrice: {
        type: Number,
        required: true
    },
    productDescription: {
        type: String,
        required: true,
        max: 550
    },
    storeName: {
        type: String,
        required: true,
        max: 225
    }
});

const executeProductModel = mongoose.model('products', productSchema);

module.exports = { executeProductModel };
