const successResponse = (
    successCode,
    successMessage,
    customCode,
    accessToken
) => {
    if (accessToken == null)
        return {
            code: successCode,
            message: successMessage,
            feedback: 'SUCCESS',
            id: customCode
        };
    return {
        code: successCode,
        message: successMessage,
        feedback: 'SUCCESS',
        id: customCode,
        access_token: accessToken
    };
};

const failedResponse = (failedCode, failedMessage, customCode) => {
    return {
        code: failedCode,
        message: failedMessage,
        feedback: 'FAILED',
        id: customCode
    };
};

module.exports = { successResponse, failedResponse };
