const SIGNUP_SUCCESS = { eng: 'Sign up successful.' };
const SIGNUP_FAILED = { eng: 'Sign up failed. Check your inputs again.' };
const LOGIN_SUCCESS = { eng: 'Login successful.' };
const LOGIN_FAILED = { eng: 'Login failed. Check your inputs again.' };
const INVALID_EMAIL = { eng: 'Email address not in use.' };
const USED_EMAIL = { eng: 'Email address already in use.' };
const INVALID_PASSWORD = { eng: 'Invalid password.' };
const CREATION_SUCCESS = { eng: 'Store created successfully.' };
const CREATION_FAILED = { eng: 'Store creation failed.' };
const INSERTION_SUCCESS = { eng: 'Product inserted to store successfully.' };
const INSERTION_FAILED = { eng: 'Product insertion to store failed.' };
const ORDER_PLACED = { eng: 'Order placed successfully.' };
const ORDER_NOT_PLACED = { eng: 'Order placing unsuccessful.' };
const NULL_TOKEN = { eng: 'Null token is passed as the argument.' };
const INVALID_TOKEN = { eng: 'Invalid token is passed as the argument.' };
const ROUTER_ERROR = { eng: 'Unhandled error occurred. Check your inputs.' };
const UPDATE_FAILED = { eng: 'Cannot update. Check your inputs again.' };
const UPDATE_SUCCESS = { eng: 'Successfully updated details.' };
const DELETION_FAILED = { eng: 'Deletion failed.' };
const DELETION_SUCCESS = { eng: 'Deleted successfully.' };
const DELETION_UNSUCCESS = { eng: 'Deleted unsuccessful.' };
const SERVER_ERROR = { eng: 'Server fault. Try again later.' };
const STORE_CREATED = { eng: 'Store created successfully.' };
const STORE_NOT_CREATED = { eng: 'Store creation unsuccessful. ' };
const STORE_AVAILABLE = { eng: 'Store name already available.' };
const STORE_NOT_AVAILABLE = { eng: 'Store not available in the database.' };
const STORE_NOT_UPDATED = { eng: 'Store details not updated. Check inputs.' };
const PRODUCT_ADDED = { eng: 'Product added successfully.' };
const PRODUCT_NOT_UPDATE = { eng: 'Product updating failed.' };
const PRODUCT_AVAILABLE = { eng: 'Product name already available.' };
const PRODUCT_UNAVAILABLE = { eng: 'No such product available in that store.' };
const INVALID_PRODUCT = { eng: 'Invalid product details. Check again.' };
const FETCHING_SUCCESS = { eng: 'Successfully fetched data from the database' };
const FETCHING_FAILED = { eng: 'Cannot fetch data from the database.' };
const INVALID_OWNER = { eng: "Don't have rights to update store details." };
const INVALID_ORDER = { eng: 'Check order details again.' };
const NO_ORDERS = { eng: "There isn't any orders placed by this user." };
const ORDER_NOT_DELETED = { eng: 'Failed to delete customer order.' };
const ORDER_DELETED = { eng: 'Delete customer order.' };
const HAVE_ORDER = { eng: 'Cannot delete vendor since a store have a order.' };

module.exports = {
    SIGNUP_SUCCESS,
    SIGNUP_FAILED,
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    INVALID_EMAIL,
    USED_EMAIL,
    INVALID_PASSWORD,
    CREATION_SUCCESS,
    CREATION_FAILED,
    INSERTION_SUCCESS,
    INSERTION_FAILED,
    ORDER_PLACED,
    ORDER_NOT_PLACED,
    NULL_TOKEN,
    INVALID_TOKEN,
    ROUTER_ERROR,
    UPDATE_FAILED,
    UPDATE_SUCCESS,
    DELETION_FAILED,
    DELETION_SUCCESS,
    DELETION_UNSUCCESS,
    SERVER_ERROR,
    STORE_CREATED,
    STORE_NOT_CREATED,
    STORE_AVAILABLE,
    STORE_NOT_AVAILABLE,
    STORE_NOT_UPDATED,
    PRODUCT_ADDED,
    PRODUCT_NOT_UPDATE,
    PRODUCT_AVAILABLE,
    PRODUCT_UNAVAILABLE,
    INVALID_PRODUCT,
    FETCHING_SUCCESS,
    FETCHING_FAILED,
    INVALID_OWNER,
    INVALID_ORDER,
    NO_ORDERS,
    ORDER_NOT_DELETED,
    ORDER_DELETED,
    HAVE_ORDER
};
