const router = require('express').Router();
const { executeStoreModel } = require('../model/storeModel');
const message = require('../constants/message');
const status = require('../constants/statusCodes');
const { emailFetcher } = require('../controller/tokenController');
const {
    failedResponse,
    successResponse
} = require('../constants/respondStatus');
const {
    vendorStoreValidation,
    vendorStoreUpdateValidation
} = require('../controller/storeController');
const { executeVendorModel } = require('../model/vendorModel');

// GET ALL STORES
/**
 * @swagger
 * /store/all:
 *  get:
 *    tags:
 *      - Store
 *    description: Get list of all stores
 *    responses:
 *      200:
 *        description: A list of all store details
 */
router.get('/all', (req, res) => {
    executeStoreModel.find({}, (error, result) => {
        if (error) {
            res.json(
                failedResponse(
                    status.CONFLICT,
                    message.FETCHING_FAILED,
                    '#S101'
                )
            );
        } else {
            res.json(result);
        }
    });
});

// CREATE A STORE
/**
 * @swagger
 * /store/add:
 *  post:
 *    tags:
 *      - Store
 *    description: Adding store to the system
 *    parameters:
 *       - name: 'Authorization'
 *         description: Vendor object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *       - name: Body
 *         description: Store object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/storeAddRequest'
 *    responses:
 *      200:
 *        description: A message with success status
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 *   storeAddRequest:
 *     properties:
 *       storeName:
 *         type: string
 *       storeCategory:
 *         type: string
 *       storeDescription:
 *         type: string
 */
router.post('/create', async (req, res) => {
    const fetchedEmail = await emailFetcher(req);
    if (fetchedEmail === false)
        return res.json(
            failedResponse(status.BAD_REQUEST, message.CREATION_FAILED, '#S201')
        );

    // checking the existence of the email
    const emailExist = await executeVendorModel.findOne({
        vendorEmail: fetchedEmail
    });

    if (!emailExist)
        return res.json(
            failedResponse(status.BAD_REQUEST, message.INVALID_EMAIL, '#S202')
        );

    const { error } = vendorStoreValidation(req.body);
    if (error)
        return res.json(
            failedResponse(
                status.BAD_REQUEST,
                message.STORE_NOT_CREATED,
                '#S203'
            )
        );

    const storeExist = await executeStoreModel.findOne({
        storeName: req.body.storeName
    });

    if (storeExist)
        return res.json(
            failedResponse(status.BAD_REQUEST, message.STORE_AVAILABLE, '#S204')
        );

    const store = new executeStoreModel({
        vendorEmail: fetchedEmail,
        storeName: req.body.storeName,
        storeCategory: req.body.storeCategory,
        storeDescription: req.body.storeDescription
    });

    const saveStore = await store.save();
    if (!saveStore)
        return res.json(
            failedResponse(
                status.BAD_REQUEST,
                message.STORE_NOT_CREATED,
                '#S205'
            )
        );
    res.json(successResponse(status.OK, message.STORE_CREATED, '#S206'));
});

// UPDATE THE STORE DETAILS
/**
 * @swagger
 * /store/update:
 *  put:
 *    tags:
 *      - Store
 *    description: Update store in the system
 *    parameters:
 *       - name: 'Authorization'
 *         description: Vendor object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *       - name: Body
 *         description: Store object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/storeUpdateRequest'
 *    responses:
 *      200:
 *        description: A message with success status
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 *   storeUpdateRequest:
 *     properties:
 *       storeCategory:
 *         type: string
 *       storeDescription:
 *         type: string
 */
router.put('/update', async (req, res) => {
    try {
        // FETCHING EMAIL FORM THE TOKEN
        const fetchedEmail = await emailFetcher(req);
        if (fetchedEmail === false)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_EMAIL,
                    '#S301'
                )
            );

        // CHECKING FOR THE EXISTENCE OF EMAIL
        const emailExist = await executeVendorModel.findOne({
            vendorEmail: fetchedEmail
        });

        if (!emailExist)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_EMAIL,
                    '#S302'
                )
            );

        // CHECKING TO THE STORE NAME IN DATABASE
        const storeDetails = await executeStoreModel.findOne({
            storeName: req.body.storeName
        });

        if (!storeDetails)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.STORE_NOT_AVAILABLE,
                    '#S303'
                )
            );

        // CHECKING FOR THE INPUT ERRORS
        const { error } = vendorStoreUpdateValidation(req.body);
        if (error)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.STORE_NOT_UPDATED,
                    '#S304'
                )
            );

        // CHECKING WHETHER LOGGED VENDOR IS THE OWNER OF THE STORE
        if (storeDetails.vendorEmail !== fetchedEmail) {
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_OWNER,
                    '#S305'
                )
            );
        }

        // UPDATE STORE DETAILS
        const updateStore = await storeDetails.updateOne({
            storeCategory: req.body.storeCategory,
            storeDescription: req.body.storeDescription
        });

        if (!updateStore)
            return res.json(
                failedResponse(status.FORBIDDEN, message.SERVER_ERROR, '#S306')
            );

        res.json(successResponse(status.OK, message.UPDATE_SUCCESS, '#S307'));
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.UPDATE_FAILED, '#S308')
        );
    }
});

// DELETE STORE
/**
 * @swagger
 * /store/delete:
 *  delete:
 *    tags:
 *      - Store
 *    description: Delete store in the system
 *    parameters:
 *       - name: 'Authorization'
 *         description: Vendor object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *       - name: Body
 *         description: Store object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/vendorDeleteRequest'
 *    responses:
 *      200:
 *        description: A message with success status
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 *   vendorDeleteRequest:
 *     properties:
 *       storeName:
 *         type: string
 */
router.delete('/delete', async (req, res) => {
    try {
        // FETCHING EMAIL FORM THE TOKEN
        const fetchedEmail = await emailFetcher(req);
        if (fetchedEmail === false)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_EMAIL,
                    '#S401'
                )
            );

        // CHECKING TO THE STORE NAME IN DATABASE
        const storeDetails = await executeStoreModel.findOne({
            storeName: req.body.storeName
        });

        if (!storeDetails)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.STORE_NOT_AVAILABLE,
                    '#S402'
                )
            );

        // CHECKING WHETHER LOGGED VENDOR IS THE OWNER OF THE STORE
        if (storeDetails.vendorEmail !== fetchedEmail) {
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_OWNER,
                    '#S403'
                )
            );
        }

        const deleteStore = await executeStoreModel.findOneAndDelete({
            storeName: req.body.storeName,
            vendorEmail: fetchedEmail
        });

        if (!deleteStore)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.DELETION_UNSUCCESS,
                    '#S404'
                )
            );

        res.json(successResponse(status.OK, message.DELETION_SUCCESS, '#S405'));
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#S406')
        );
    }
});

module.exports = router;
