const router = require('express').Router();
const { executeProductModel } = require('../model/productModel');
const { executeStoreModel } = require('../model/storeModel');
const { executeVendorModel } = require('../model/vendorModel');
const message = require('../constants/message');
const status = require('../constants/statusCodes');
const { emailFetcher } = require('../controller/tokenController');
const {
    failedResponse,
    successResponse
} = require('../constants/respondStatus');
const {
    vendorProductValidation,
    vendorProductUpdateValidation
} = require('../controller/productController');

// GET ALL PRODUCTS
/**
 * @swagger
 * /product/all:
 *  get:
 *    tags:
 *      - Product
 *    description: Get list of all products
 *    responses:
 *      200:
 *        description: A list of all product details
 */
router.get('/all', (req, res) => {
    executeProductModel.find({}, (error, result) => {
        if (error) {
            res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.FETCHING_FAILED,
                    '#P101'
                )
            );
        } else {
            res.json(result);
        }
    });
});

// INSERTING PRODUCT
/**
 * @swagger
 * /product/add:
 *  post:
 *    tags:
 *      - Product
 *    description: Adding product to the system
 *    parameters:
 *       - name: 'Authorization'
 *         description: Vendor object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *       - name: Body
 *         description: Product object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/productAddRequest'
 *    responses:
 *      200:
 *        description: A message with success status
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 *   productAddRequest:
 *     properties:
 *       storeName:
 *         type: string
 *       productName:
 *         type: string
 *       productImage:
 *         type: string
 *       productQuantity:
 *         type: number
 *       productPrice:
 *         type: number
 *       productDescription:
 *         type: string
 */
router.post('/add', async (req, res) => {
    try {
        const fetchedEmail = await emailFetcher(req);
        if (fetchedEmail === false)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.CREATION_FAILED,
                    '#P201'
                )
            );

        // checking the existence of the email
        const emailExist = await executeVendorModel.findOne({
            vendorEmail: fetchedEmail
        });

        if (!emailExist)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_EMAIL,
                    '#P202'
                )
            );

        const storeDetails = await executeStoreModel.findOne({
            storeName: req.body.storeName
        });

        if (!storeDetails)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.STORE_NOT_AVAILABLE,
                    '#P203'
                )
            );

        // CHECKING WHETHER LOGGED VENDOR IS THE OWNER OF THE STORE
        if (storeDetails.vendorEmail !== fetchedEmail)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_OWNER,
                    '#P204'
                )
            );

        // CHECKING WHETHER ONE PRODUCT AVAILABLE IN THE SAME STORE WITH SAME NAME
        const isProduct = await executeProductModel.findOne({
            productName: req.body.productName,
            storeName: req.body.storeName
        });

        if (isProduct)
            return res.json(
                failedResponse(
                    status.FORBIDDEN,
                    message.PRODUCT_AVAILABLE,
                    '#P205'
                )
            );

        // CHECKING FOR THE INPUT ERRORS
        const { error } = vendorProductValidation(req.body);
        if (error)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_PRODUCT,
                    '#P206'
                )
            );

        const product = new executeProductModel({
            storeName: req.body.storeName,
            productName: req.body.productName,
            productImage: req.body.productImage,
            productQuantity: req.body.productQuantity,
            productPrice: req.body.productPrice,
            productDescription: req.body.productDescription
        });

        const saveProduct = await product.save();

        if (!saveProduct)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.PRODUCT_NOT_ADDED,
                    '#P207'
                )
            );

        res.json(successResponse(status.OK, message.PRODUCT_ADDED, '#P208'));
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#P209')
        );
    }
});

// UPDATING PRODUCT
/**
 * @swagger
 * /product/update:
 *  put:
 *    tags:
 *      - Product
 *    description: Update product in the system
 *    parameters:
 *       - name: 'Authorization'
 *         description: Vendor object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *       - name: Body
 *         description: Product object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/productUpdateRequest'
 *    responses:
 *      200:
 *        description: A message with success status
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 *   productUpdateRequest:
 *     properties:
 *       storeName:
 *         type: string
 *       productName:
 *         type: string
 *       productImage:
 *         type: string
 *       productQuantity:
 *         type: number
 *       productPrice:
 *         type: number
 *       productDescription:
 *         type: string
 */
router.put('/update', async (req, res) => {
    try {
        // FETCHING EMAIL FORM THE TOKEN
        const fetchedEmail = await emailFetcher(req);
        if (fetchedEmail === false)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_EMAIL,
                    '#P301'
                )
            );

        // checking the existence of the email
        const emailExist = await executeVendorModel.findOne({
            vendorEmail: fetchedEmail
        });

        if (!emailExist)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_EMAIL,
                    '#P302'
                )
            );

        // CHECKING TO THE STORE NAME IN DATABASE
        const storeDetails = await executeStoreModel.findOne({
            storeName: req.body.storeName
        });

        if (!storeDetails)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.STORE_NOT_AVAILABLE,
                    '#P303'
                )
            );

        // CHECKING WHETHER LOGGED VENDOR IS THE OWNER OF THE STORE
        if (storeDetails.vendorEmail !== fetchedEmail) {
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_OWNER,
                    '#P304'
                )
            );
        }

        const productDetails = await executeProductModel.findOne({
            productName: req.body.productName,
            storeName: req.body.storeName
        });

        if (!productDetails)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.PRODUCT_UNAVAILABLE,
                    '#P305'
                )
            );
        const { error } = vendorProductUpdateValidation(req.body);
        if (error)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.PRODUCT_NOT_UPDATE,
                    '#P306'
                )
            );
        const updateProduct = await productDetails.updateOne({
            productImage: req.body.productImage,
            productQuantity: req.body.productQuantity,
            productPrice: req.body.productPrice
        });
        if (!updateProduct)
            return res.json(
                failedResponse(status.FORBIDDEN, message.SERVER_ERROR, '#P307')
            );

        res.json(successResponse(status.OK, message.UPDATE_SUCCESS, '#P308'));
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.UPDATE_FAILED, '#P309')
        );
    }
});

// DELETING PRODUCT
/**
 * @swagger
 * /product/delete:
 *  delete:
 *    tags:
 *      - Product
 *    description: Delete product in the system
 *    parameters:
 *       - name: 'Authorization'
 *         description: Vendor object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *       - name: Body
 *         description: Product object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/productDeleteRequest'
 *    responses:
 *      200:
 *        description: A message with success status
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 *   productDeleteRequest:
 *     properties:
 *       storeName:
 *         type: string
 *       productName:
 *         type: string
 */
router.delete('/delete', async (req, res) => {
    try {
        // FETCHING EMAIL FORM THE TOKEN
        const fetchedEmail = await emailFetcher(req);
        if (fetchedEmail === false)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_EMAIL,
                    '#P401'
                )
            );
        // CHECKING TO THE STORE NAME IN DATABASE
        const storeDetails = await executeStoreModel.findOne({
            storeName: req.body.storeName
        });
        if (!storeDetails)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.STORE_NOT_AVAILABLE,
                    '#P402'
                )
            );

        // CHECKING WHETHER LOGGED VENDOR IS THE OWNER OF THE STORE
        if (storeDetails.vendorEmail !== fetchedEmail) {
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_OWNER,
                    '#P403'
                )
            );
        }

        const deleteStore = await executeProductModel.findOneAndDelete({
            storeName: req.body.storeName,
            productName: req.body.productName
        });

        if (!deleteStore)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.DELETION_UNSUCCESS,
                    '#P405'
                )
            );

        res.json(successResponse(status.OK, message.DELETION_SUCCESS, '#P406'));
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.DELETION_FAILED, '#P407')
        );
    }
});

module.exports = router;
