const router = require('express').Router();
const { executeCustomerModel } = require('../model/customerModel');
const message = require('../constants/message');
const status = require('../constants/statusCodes');
const JWT = require('jsonwebtoken');
const { TOKEN_KEY } = require('../constants/config');
const { customerEmailFetcher } = require('../controller/tokenController');
const {
    customerRegisterValidation,
    customerLoginValidation,
    customerUpdateValidation
} = require('../controller/customerController');
const {
    successResponse,
    failedResponse
} = require('../constants/respondStatus');
const {
    passwordEncrypt,
    passwordDecrypt
} = require('../controller/passwordSecure');

// GET ALL CUSTOMER DETAILS
/**
 * @swagger
 * /customer/all:
 *  get:
 *    tags:
 *      - Customer
 *    description: Get list of all customers
 *    responses:
 *      200:
 *        description: A list of all customer details
 */
router.get('/all', (req, res) => {
    try {
        executeCustomerModel.find({}, (error, result) => {
            if (error) {
                res.json(
                    failedResponse(
                        status.CONFLICT,
                        message.FETCHING_FAILED,
                        '#C101'
                    )
                );
            } else {
                res.json(result);
            }
        });
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#C102')
        );
    }
});

// CUSTOMER REGISTER
/**
 * @swagger
 * /customer/register:
 *  post:
 *    tags:
 *      - Customer
 *    description: Authenticate customer to register in the system
 *    parameters:
 *       - name: Body
 *         description: Customer object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/customerRegisterRequest'
 *    responses:
 *      200:
 *        description: An access token that contains customer email
 * definitions:
 *   customerRegisterRequest:
 *     properties:
 *       customerName:
 *         type: string
 *       customerEmail:
 *         type: string
 *       customerPassword:
 *         type: string
 *       customerMobile:
 *         type: string
 *       customerAddress:
 *         type: string
 */
router.post('/register', async (req, res) => {
    try {
        // validate data using Joi
        const { error } = customerRegisterValidation(req.body);
        if (error)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.SIGNUP_FAILED,
                    '#C201'
                )
            );

        // checking the existence of the email
        const emailExist = await executeCustomerModel.findOne({
            customerEmail: req.body.customerEmail
        });

        if (emailExist)
            return res.json(
                failedResponse(status.BAD_REQUEST, message.USED_EMAIL, '#C202')
            );

        // encrypt password using crypto
        const password = req.body.customerPassword;
        const encryptedPassword = passwordEncrypt(password);

        // save a new vendor
        const customer = new executeCustomerModel({
            customerEmail: req.body.customerEmail,
            customerName: req.body.customerName,
            customerPassword: encryptedPassword,
            customerMobile: req.body.customerMobile,
            customerAddress: req.body.customerAddress
        });

        // Saving process validation
        const savingCustomer = await customer.save();
        if (!savingCustomer)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.SIGNUP_FAILED,
                    '#C203'
                )
            );

        // create a token and pass that token as the response
        const token = JWT.sign(
            { _id: customer._id, customerEmail: customer.customerEmail },
            TOKEN_KEY
        );
        res.json(
            successResponse(status.OK, message.SIGNUP_SUCCESS, '#C204', token)
        );
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#C205')
        );
    }
});

// CUSTOMER LOGIN
/**
 * @swagger
 * /customer/login:
 *  post:
 *    tags:
 *      - Customer
 *    description: Authenticate customer to login the system
 *    parameters:
 *       - name: Body
 *         description: Customer object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/customerLoginRequest'
 *    responses:
 *      200:
 *        description: An access token that contains customer email
 * definitions:
 *   customerLoginRequest:
 *     properties:
 *       customerEmail:
 *         type: string
 *       customerPassword:
 *         type: string
 */
router.post('/login', async (req, res) => {
    try {
        const { error } = customerLoginValidation(req.body);
        if (error)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.LOGIN_FAILED,
                    '#C301'
                )
            );

        const customer = await executeCustomerModel.findOne({
            customerEmail: req.body.customerEmail
        });
        if (!customer)
            return res.json(
                failedResponse(status.FORBIDDEN, message.INVALID_EMAIL, '#C302')
            );

        const enteredPassword = req.body.customerPassword;
        const storedPassword = customer.customerPassword;

        // decrypt stored password
        const decryptedStoredPassword = passwordDecrypt(storedPassword);

        // compare entered password with password stored in the database
        if (enteredPassword !== decryptedStoredPassword)
            res.json(
                failedResponse(
                    status.FORBIDDEN,
                    message.INVALID_PASSWORD,
                    '#C303'
                )
            );
        // create a token and pass that token as the response
        const token = JWT.sign(
            { _id: customer._id, customerEmail: customer.customerEmail },
            TOKEN_KEY
        );
        res.json(
            successResponse(status.OK, message.LOGIN_SUCCESS, '#C304', token)
        );
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#C305')
        );
    }
});

// CUSTOMER UPDATE
/**
 * @swagger
 * /customer/update:
 *  put:
 *    tags:
 *      - Customer
 *    description: Allow customer to update the system
 *    parameters:
 *       - name: 'Authorization'
 *         description: Customer object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/customerUpdateRequest'
 *    responses:
 *      200:
 *        description: A message with success status
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 *   customerUpdateRequest:
 *     properties:
 *       customerName:
 *         type: string
 *       customerMobile:
 *         type: string
 *       customerAddress:
 *         type: string
 */
router.put('/update', async (req, res) => {
    try {
        const fetchedEmail = await customerEmailFetcher(req);
        if (fetchedEmail === false)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.UPDATE_FAILED,
                    '#C401'
                )
            );

        const customerDetails = await executeCustomerModel.findOne({
            customerEmail: fetchedEmail
        });
        if (!customerDetails)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_TOKEN,
                    '#C402'
                )
            );

        const validatedCustomer = customerUpdateValidation(req.body);
        if (!validatedCustomer)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_EMAIL,
                    '#C403'
                )
            );

        const updateCustomer = await customerDetails.updateOne({
            customerName: req.body.customerName,
            customerMobile: req.body.customerMobile,
            customerAddress: req.body.customerAddress
        });
        if (!updateCustomer)
            return res.json(
                failedResponse(status.FORBIDDEN, message.SERVER_ERROR, '#C404')
            );
        res.json(successResponse(status.OK, message.UPDATE_SUCCESS, '#C405'));
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#C406')
        );
    }
});

// CUSTOMER DELETE
/**
 * @swagger
 * /customer/delete:
 *  delete:
 *    tags:
 *      - Customer
 *    description: Allow customer to delete system profile
 *    parameters:
 *       - name: 'Authorization'
 *         description: Customer object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *    responses:
 *      200:
 *        description: A message with success status
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 */
router.delete('/delete', async (req, res) => {
    try {
        const fetchedEmail = customerEmailFetcher(req);
        if (fetchedEmail === false)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_EMAIL,
                    '#C501'
                )
            );

        const deleteCustomer = await executeCustomerModel.findOneAndDelete({
            customerEmail: fetchedEmail
        });
        if (!deleteCustomer)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.DELETION_UNSUCCESS,
                    '#C502'
                )
            );
        res.json(successResponse(status.OK, message.DELETION_SUCCESS, '#C503'));
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#C504')
        );
    }
});

module.exports = router;
