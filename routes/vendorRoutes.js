const router = require('express').Router();
const { executeVendorModel } = require('../model/vendorModel');
const message = require('../constants/message');
const status = require('../constants/statusCodes');
const JWT = require('jsonwebtoken');
const { TOKEN_KEY } = require('../constants/config');
const { emailFetcher } = require('../controller/tokenController');
const {
    vendorRegisterValidation,
    vendorLoginValidation,
    vendorUpdateValidation
} = require('../controller/vendorController');
const {
    successResponse,
    failedResponse
} = require('../constants/respondStatus');
const {
    passwordEncrypt,
    passwordDecrypt
} = require('../controller/passwordSecure');

// VENDOR REGISTER API
/**
 * @swagger
 * /vendor/register:
 *  post:
 *    tags:
 *      - Vendor
 *    description: Authenticate vendor to register in the system
 *    parameters:
 *       - name: Body
 *         description: Vendor object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/vendorRegisterRequest'
 *    responses:
 *      200:
 *        description: An access token that contains vendor email
 * definitions:
 *   vendorRegisterRequest:
 *     properties:
 *       vendorName:
 *         type: string
 *       vendorEmail:
 *         type: string
 *       vendorPassword:
 *         type: string
 *       vendorMobile:
 *         type: string
 *       vendorAddress:
 *         type: string
 */
router.post('/register', async (req, res) => {
    try {
        // validate data using Joi
        const { error } = vendorRegisterValidation(req.body);
        if (error)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.SIGNUP_FAILED,
                    '#V101'
                )
            );
        // checking the existence of the email
        const emailExist = await executeVendorModel.findOne({
            vendorEmail: req.body.vendorEmail
        });

        if (emailExist)
            return res.json(
                failedResponse(status.BAD_REQUEST, message.USED_EMAIL, '#V102')
            );

        // encrypt password using crypto
        const password = req.body.vendorPassword;
        const encryptedPassword = passwordEncrypt(password);

        // save a new vendor
        const vendor = new executeVendorModel({
            vendorEmail: req.body.vendorEmail,
            vendorName: req.body.vendorName,
            vendorPassword: encryptedPassword,
            vendorMobile: req.body.vendorMobile,
            vendorAddress: req.body.vendorAddress
        });

        // Saving process validation
        const savingUser = await vendor.save();
        if (!savingUser)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.SIGNUP_FAILED,
                    '#V103'
                )
            );
        // create a token and pass that token as the response
        const token = JWT.sign(
            { _id: vendor._id, vendorEmail: vendor.vendorEmail },
            TOKEN_KEY
        );
        res.json(
            successResponse(status.OK, message.SIGNUP_SUCCESS, '#V104', token)
        );
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#V105')
        );
    }
});

// VENDOR LOGIN API
/**
 * @swagger
 * /vendor/login:
 *  post:
 *    tags:
 *      - Vendor
 *    description: Authenticate vendor to login the system
 *    parameters:
 *       - name: Body
 *         description: Vendor object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/vendorLoginRequest'
 *    responses:
 *      200:
 *        description: An access token that contains vendor email
 * definitions:
 *   vendorLoginRequest:
 *     properties:
 *       vendorEmail:
 *         type: string
 *       vendorPassword:
 *         type: string
 */
router.post('/login', async (req, res) => {
    try {
        const { error } = vendorLoginValidation(req.body);
        if (error)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.LOGIN_FAILED,
                    '#V201'
                )
            );

        const vendor = await executeVendorModel.findOne({
            vendorEmail: req.body.vendorEmail
        });
        if (!vendor)
            return res.json(
                failedResponse(status.FORBIDDEN, message.INVALID_EMAIL, '#V202')
            );

        const enteredPassword = req.body.vendorPassword;
        const storedPassword = vendor.vendorPassword;

        // decrypt stored password
        const decryptedStoredPassword = passwordDecrypt(storedPassword);

        // compare entered password with password stored in the database
        if (enteredPassword !== decryptedStoredPassword)
            res.json(
                failedResponse(
                    status.FORBIDDEN,
                    message.INVALID_PASSWORD,
                    '#V203'
                )
            );
        // create a token and pass that token as the response
        const token = JWT.sign(
            { _id: vendor._id, vendorEmail: vendor.vendorEmail },
            TOKEN_KEY
        );
        res.json(
            successResponse(status.OK, message.LOGIN_SUCCESS, '#V204', token)
        );
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#V205')
        );
    }
});

// UPDATE VENDOR DETAILS
/**
 * @swagger
 * /vendor/update:
 *  put:
 *    tags:
 *      - Vendor
 *    description: Allow vendor to update the system
 *    parameters:
 *       - name: 'Authorization'
 *         description: Vendor object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *       - name: Body
 *         description: User object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/vendorUpdateRequest'
 *    responses:
 *      200:
 *        description: A message with success status
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 *   vendorUpdateRequest:
 *     properties:
 *       vendorName:
 *         type: string
 *       vendorMobile:
 *         type: string
 *       vendorAddress:
 *         type: string
 */
router.put('/update', async (req, res) => {
    try {
        const fetchedEmail = await emailFetcher(req);
        if (fetchedEmail === false)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.UPDATE_FAILED,
                    '#V301'
                )
            );

        const vendorDetails = await executeVendorModel.findOne({
            vendorEmail: fetchedEmail
        });

        if (!vendorDetails)
            return req.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_EMAIL,
                    '#302'
                )
            );

        const validatedName = vendorUpdateValidation(req.body);
        if (!validatedName)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.UPDATE_FAILED,
                    '#V303'
                )
            );

        const updateVendor = await vendorDetails.updateOne({
            vendorName: req.body.vendorName,
            vendorMobile: req.body.vendorMobile,
            vendorAddress: req.body.vendorAddress
        });
        if (!updateVendor)
            return res.json(
                failedResponse(status.FORBIDDEN, message.SERVER_ERROR, '#V304')
            );
        res.json(successResponse(status.OK, message.UPDATE_SUCCESS, '#V305'));
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#V306')
        );
    }
});

// DELETE VENDOR
/**
 * @swagger
 * /vendor/delete:
 *  delete:
 *    tags:
 *      - Vendor
 *    description: Allow vendor to delete system profile
 *    parameters:
 *       - name: 'Authorization'
 *         description: Customer object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *    responses:
 *      200:
 *        description: A message with success status
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 */
router.delete('/delete', async (req, res) => {
    try {
        const fetchedEmail = emailFetcher(req);
        if (fetchedEmail === false)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_EMAIL,
                    '#V401'
                )
            );

        const deleteVendor = await executeVendorModel.findOneAndDelete({
            vendorEmail: fetchedEmail
        });
        if (!deleteVendor)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.DELETION_UNSUCCESS,
                    '#V402'
                )
            );
        res.json(successResponse(status.OK, message.DELETION_SUCCESS, '#V403'));
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#V404')
        );
    }
});

// LIST OF ALL VENDORS
/**
 * @swagger
 * /vendor/all:
 *  get:
 *    tags:
 *      - Vendor
 *    description: Get list of all vendors
 *    responses:
 *      200:
 *        description: A list of all vendors details
 */
router.get('/all', (req, res) => {
    try {
        executeVendorModel.find({}, (error, result) => {
            if (error) {
                res.json(
                    failedResponse(
                        status.CONFLICT,
                        message.FETCHING_FAILED,
                        '#V501'
                    )
                );
            } else {
                res.json(result);
            }
        });
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#V502')
        );
    }
});

module.exports = router;
