const router = require('express').Router();
const { executeOrderModel } = require('../model/orderModel');
const { executeCustomerModel } = require('../model/customerModel');
const message = require('../constants/message');
const status = require('../constants/statusCodes');
const { customerEmailFetcher } = require('../controller/tokenController');
const {
    checkingItemAvailable,
    orderValidation,
    totalCalculator,
    afterOrderProductUpdate
} = require('../controller/orderController');
const {
    successResponse,
    failedResponse
} = require('../constants/respondStatus');

// GET ALL ORDER DETAILS
/**
 * @swagger
 * /order/all:
 *  get:
 *    tags:
 *      - Order
 *    description: Get list of all orders
 *    responses:
 *      200:
 *        description: A list of all order details
 */
router.get('/all', (req, res) => {
    try {
        executeOrderModel.find({}, (error, result) => {
            if (error) {
                res.json(
                    failedResponse(
                        status.CONFLICT,
                        message.FETCHING_FAILED,
                        '#O101'
                    )
                );
            } else {
                res.json(result);
            }
        });
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#O102')
        );
    }
});

// ORDER PLACE
/**
 * @swagger
 * /order/place:
 *  post:
 *    tags:
 *      - Order
 *    description: Adding order to the system
 *    parameters:
 *       - name: 'Authorization'
 *         description: Customer object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *       - name: Body
 *         description: Order object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/orderAddRequest'
 *    responses:
 *      200:
 *        description: A message with success status
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 *   orderAddRequest:
 *     properties:
 *       storeName:
 *         type: string
 *       orderItems:
 *         type: object
 */
router.post('/place', async (req, res) => {
    try {
        const fetchedEmail = await customerEmailFetcher(req);
        if (fetchedEmail === false)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.UPDATE_FAILED,
                    '#O201'
                )
            );

        const customerDetails = await executeCustomerModel.findOne({
            customerEmail: fetchedEmail
        });
        if (!customerDetails)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_TOKEN,
                    '#O202'
                )
            );

        const { error } = await orderValidation(req.body);
        if (error)
            res.json(
                failedResponse(
                    status.BAD_GATEWAY,
                    message.ORDER_NOT_PLACED,
                    '#O203'
                )
            );

        let bodyItems = req.body.orderItems;

        let orderStatusValid = true;

        for (let itemName in bodyItems) {
            // if (bodyItems.hasOwnProperty(itemName)) {
            const itemQuantity = bodyItems[itemName];
            const isAvailable = await checkingItemAvailable(
                req.body.storeName,
                itemName,
                itemQuantity
            );
            if (isAvailable === false) {
                orderStatusValid = false;
                res.json(
                    failedResponse(
                        status.BAD_GATEWAY,
                        message.INVALID_ORDER,
                        '#O204'
                    )
                );
                break;
            }
            // }
        }
        if (orderStatusValid === true) {
            let entireTotal = 0;
            for (let itemName in bodyItems) {
                // if (bodyItems.hasOwnProperty(itemName)) {
                const itemQuantity = bodyItems[itemName];
                const currentTotal = await totalCalculator(
                    req.body.storeName,
                    itemName,
                    itemQuantity
                );
                entireTotal += currentTotal;
                // }
            }
            // console.log('Full total is: ' + entireTotal);

            const order = new executeOrderModel({
                customerEmail: fetchedEmail,
                storeName: req.body.storeName,
                orderProducts: JSON.stringify(req.body.orderItems),
                orderTotal: entireTotal,
                placedDate: new Date().toString()
            });

            await order.save();

            for (let itemName in bodyItems) {
                const itemQuantity = bodyItems[itemName];
                const isUpdate = await afterOrderProductUpdate(
                    req.body.storeName,
                    itemName,
                    itemQuantity
                );
            }

            res.json(successResponse(status.OK, message.ORDER_PLACED, '#O205'));
        }
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#O206')
        );
    }
});

// FETCH ALL THE ORDER DETAILS REGARDING TO A CUSTOMER
/**
 * @swagger
 * /order/customerOrder:
 *  get:
 *    tags:
 *      - Order
 *    description: Getting all the orders related to particular customer
 *    parameters:
 *       - name: 'Authorization'
 *         description: Customer object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *    responses:
 *      200:
 *        description: JSON with all order details regarding to a customer
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 */
router.get('/customerOrder', async (req, res) => {
    try {
        const fetchedEmail = await customerEmailFetcher(req);
        if (fetchedEmail === false)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_TOKEN,
                    '#O301'
                )
            );

        const customerDetails = await executeCustomerModel.findOne({
            customerEmail: fetchedEmail
        });
        if (!customerDetails)
            res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_TOKEN,
                    '#O302'
                )
            );
        const allCustomerOrders = await executeOrderModel.find({
            customerEmail: fetchedEmail
        });
        if (!allCustomerOrders)
            return res.json(
                failedResponse(status.BAD_REQUEST, message.NO_ORDERS, '#O303')
            );

        res.json(allCustomerOrders);
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#O304')
        );
    }
});

// ORDER RECEIVED
/**
 * @swagger
 * /order/receive:
 *  delete:
 *    tags:
 *      - Order
 *    description: Delete order from the system after receiving
 *    parameters:
 *       - name: 'Authorization'
 *         description: Customer object
 *         in: header
 *         required: true
 *         schema:
 *           $ref: '#/definitions/requestHeader'
 *       - name: Body
 *         description: Order object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/orderReceiveRequest'
 *    responses:
 *      200:
 *        description: A message with success status
 * definitions:
 *   requestHeader:
 *     properties:
 *       Authorization:
 *         type: string
 *   orderReceiveRequest:
 *     properties:
 *       orderId:
 *         type: string
 */
router.delete('/receive', async (req, res) => {
    try {
        const fetchedEmail = await customerEmailFetcher(req);
        if (fetchedEmail === false)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_TOKEN,
                    '#O401'
                )
            );
        const customerDetails = await executeCustomerModel.findOne({
            customerEmail: fetchedEmail
        });
        if (!customerDetails)
            res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_TOKEN,
                    '#O402'
                )
            );

        const customerOrder = await executeOrderModel.findOne({
            customerEmail: fetchedEmail,
            _id: req.body.orderId
        });
        if (!customerOrder)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.INVALID_ORDER,
                    '#O403'
                )
            );

        const deleteOrder = await executeOrderModel.findOneAndDelete({
            customerEmail: fetchedEmail,
            _id: req.body.orderId
        });
        if (!deleteOrder)
            return res.json(
                failedResponse(
                    status.BAD_REQUEST,
                    message.ORDER_NOT_DELETED,
                    '#O404'
                )
            );

        res.json(successResponse(status.OK, message.ORDER_DELETED, '#O405'));
    } catch (error) {
        res.json(
            failedResponse(status.CONFLICT, message.ROUTER_ERROR, '#O406')
        );
    }
});

module.exports = router;
